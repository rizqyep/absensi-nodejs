const express = require('express');

const router = express.Router();
const absenController = require('../controllers/absenController');

router.post('/', absenController.add);
router.get('/', absenController.create);
router.put('/', absenController.update);
router.delete('/', absenController.deleteUser);

module.exports = router;
