//Required Libs
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//App Instances
const app = express();


const Absen = require('./models/Absen');
const { render } = require('ejs');
const methodOverride = require('method-override');
const absenRoutes = require('./routes/absenRoutes');
// ! MVC error, so i decided to use crud without MVC & also still keep the mvc files

const mongooseURI =
    'mongodb://ical:ical123@absen-shard-00-00.p0fyh.mongodb.net:27017,absen-shard-00-01.p0fyh.mongodb.net:27017,absen-shard-00-02.p0fyh.mongodb.net:27017/data_absen?ssl=true&replicaSet=atlas-hgh1ip-shard-0&authSource=admin&retryWrites=true&w=majority';

mongoose
    .connect(mongooseURI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        app.listen(4444); // listen for requests
        console.log("Connected");
    })
    .catch((err) => console.log(err));

app.set('view engine', 'ejs'); // view engine
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride('_method'));

const absenController = require('./controllers/absenController');
app.get('/', absenController.index);

app.use('/absen', absenRoutes);