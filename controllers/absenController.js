const Absen = require('../models/Absen');

const add = async (req, res) => {
	const newUser = await Absen.create(req.body);
	res.send(newUser);
};

const index = (req, res) => {
	Absen.find().then((absens) => {
		const title = 'Halaman Utama';
		res.render('index', { absens, title });
	});
};

const update = async (req, res) => {
	const { id, nama, kelas, noabsen } = req.body;

	try {
		await Absen.findOneAndUpdate(
			id,
			{
				nama,
				kelas,
				noabsen,
			},
			{
				new: true,
				useFindAndModify: false,
			},
		);

		res.redirect('/');
	} catch (err) {
		res.send(err);
	}
};

const create = (req, res) => {
	const title = 'Tambahkan User';
	res.render('absens', { title });
};

const deleteUser = (req, res) => {
	const { id } = req.body;
	Absen.findByIdAndDelete(id)
		.then(() => {
			res.redirect('/');
		})
		.catch((err) => {
			res.send(err);
		});
};

module.exports = {
	add,
	index,
	create,
	update,
	deleteUser,
};
